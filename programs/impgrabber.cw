#
# Core Wars program
#
# Walter Hofmann, March 2000
#

	title "Imp grabber"
        author "Walter Hofmann <walterh@gmx.de>"

A:	data 0
K1:	data 0
	data 0
K2:	data 0
X:	data 0
	fork C
B:	equal A,0
	jump B
K:	move J1,K1
	move J2,K2
P:	move 0,A
	jump B
	data 0
M:	info 0,L
	add -100,L
J1:	jump B1
J2:	jump B2
	data 0
B2:	data 0
B1:	data 0
Q:	own C
C:	own B
	jump M
D:	movei E,F
	move &H,E
	move J1,K1
	loop L,D
	move Q,P
	move Q,B
	move 0,D
	data 0
E:	data &H
F:	data &G
L:	data 0
H:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
G:	data 0
