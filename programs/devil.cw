# Based on "Santa's little helper", but works on the reverse, and uses
# the communication cell to disable both Santa and his "little helper"

	Title "The devil"
	Author "Idan Sofer <sofer@ibm.net>"
	
A:	data 0
B:      data 59
C:	data 9
D:      data 0
CODE:	data 47295
L:	move [B],A
	equal A, CODE
	jump M
	jump L
M:	add 1,B
	move [B],A
KILL:	add 5,A
	move 0,[A]
	add -5,A
	move 0,[A]
