#
# Core Wars program
#
# Walter Hofmann, March 2000
#

	title "Throw'n'Catch"
        author "Walter Hofmann <walterh@gmx.de>"

	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	1
	data	0

L:	add	-78, TO
	equal	[TO], 0
	jump	L
	own	[TO]
	jump	L
	move	FB, [TO]
	add	1, TO
	move	IMP,[TO]
	loop	IC, L
	jump	PLACE
TO:	data	&L
IC:	data	200
FB:	fork	PLACE

PLACE:	fork	IMP

L1:	move	BOMB, PLACE
	jump	L1

BOMB:	jump	DST
	
	own	L1
	data	0
	fork	DST
DST:	fork	F2
F2:	fork	F3
F3:	fork	F4
F4:	move	BOMB, PLACE
	loop	COUNT, F4
	move	1000, COUNT
DIE:	fork	DIE
	jump	DIE
COUNT:	data	5000
	
	data	0
	
IMP:	move	IMP, NEXT
NEXT:	data	0
