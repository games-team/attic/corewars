#
# Core Wars program
#
# Static Chaos, 2000
#
# Note: This program was written to freeze Angels of Death (and similar
# programs that try to run many threads, including itself... :-) (it can
# even kill Clone... :-)
#
Title "Black Hole"
Author "Static Chaos <schaos@freemail.hu>"

CNT:   data 0
D1:    data 0
D2:    data 0
NEW:   data 0
ENTRY: jump SPAWN
TEST:  info 4, CNT
       less CNT, 3
       jump SPAWN
ENT2:  move 20, CNT
NEXT:  info 2, D1
       own [D1]
       loop CNT, NEXT
       move HOLE, [D1]
       loop CNT, NEXT
       jump TEST
HOLE:  jump HOLE
SPAWN: move 10, CNT
       move &TEST, D1
       info 2, D2
       move D2, NEW
SP2:   movei D1, D2
       movei D1, D2
       loop CNT, SP2
       fork [NEW]
       jump ENT2
