#
# A simple Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Harmless"
        author "Walter Hofmann <walterh@gmx.de>"

START:	movei A, B	# select next cell
	jump START	# an endless loop
A:	data &C
B:	data &C
C:	data 0
