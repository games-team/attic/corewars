#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Run away"
        author "Walter Hofmann <walterh@gmx.de>"

A:	data 123
	data 0
	data 0
B:	data 123
L:	equal A, B
	jump L
START:	move START, NEXT	# move on
NEXT:	data 0

