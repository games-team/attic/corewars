# Agent - Your personal executor, get's address+data from other proggies and 
# together with the sample "client" can outform most bombers in scores


        Title "Agent"
	Author "Idan Sofer <sofer@ibm.net>"
A:      data 0
PA:     data 78
B:      data 0
PB:	data 79
C:      data 0
D:      data 8
E:      data -3
L:      move &A, [PA]
P0:	move &B, [PB]
S:      move B,[A] 	# The main code
T:      jump S
