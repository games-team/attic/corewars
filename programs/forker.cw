#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Forker"
        author "Walter Hofmann <walterh@gmx.de>"

L:	info 2, B
	fork [B]
	jump L

B:	data 0
