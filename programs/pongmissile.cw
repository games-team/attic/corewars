#
# Pong Missile Launcher
#

title "Pong Missile"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

CH:			data 0	# choice
P:				data &M  # missile position
C2:			data 0	# 1/2 mem
C4:			data 0	# 1/4 mem
RP:			data 0	# +row size
RN:			data 0	# -row size
INIT:			info 1,RP
				neg RP, RN
				info 0, C2
				div 2, C2
				move C2, C4
				div 2, C4
				jump RIGHTCHG
UP:			info 2, [P]
				add RN, P
				equal [P], 0
				jump UP
				own [P]
				add RP, P
				info 2, [P]
UPCHG:		info 2, CH
				less C2, CH
				jump UP
				less CH, C4
				jump LEFT
				jump RIGHT						
DOWN:			info 2, [P]
				add RP, P
				equal [P], 0
				jump DOWN
				own [P]
				add RN, P
				info 2, [P]
DOWNCHG:		info 2, CH
				less C2, CH
				jump DOWN
				less CH, C4
				jump RIGHT
				jump LEFT						
LEFT:			info 2, [P]
				add -1, P
				equal [P], 0
				jump LEFT
				own [P]
				add 1, P
				info 2, [P]
LEFTCHG:		info 2, CH
				less C2, CH
				jump LEFT
				less CH, C4
				jump DOWN
				jump UP						
RIGHT:		info 2, [P]
				add 1, P
				equal [P], 0
				jump RIGHT
				own [P]
				add -1, P
				info 2, [P]
RIGHTCHG:	info 2, CH
				less C2, CH
				jump RIGHT
				less CH, C4
				jump UP
				jump DOWN									
M:				data 1	# the missile
			
			
