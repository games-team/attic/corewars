#
# Alexey Vyskubov <alexey@pepper.spb.ru>, March 2000
#

title "Parasito Walking"
author "Alexey Vyskubov <alexey@pepper.spb.ru>"

START:	info 2, A
F:	fork [A]
	add 1, A
	jump F
A:	data 0
