
# By Zinx Verituse <zinx@linuxfreak.com>
# It clones more than you think :/

title "Clone"
author "Zinx Verituse <zinx@linuxfreak.com>"

S:	info 2, N
	move 4, D
	move &S, P
C:	movei P, N
	movei P, N
	loop D, C
J:	add -8, N
	fork [N]
	jump S

P:	data &S
D:	data 4
N:	data 0
