#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Back walker"
        author "Walter Hofmann <walterh@gmx.de>"

END:	data 0
	data 0
START:  move &START, A
	move &END, B
	move 6, C
L:	movei A, B
	loop C, L
	jump END
A:      data 0
B:      data 0
C:      data 0
