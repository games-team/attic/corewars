# Name    : Angles of Death
# Author  : Jan Pool
#           http://espresso.ee.sun.ac.za/~jpool/
# Version : 0.0.1 (06/03/2000)
#	    The 'Angles of Death' is my first replicator. The first copy 
#	    start to create new copies up to four. If four copies exist
#	    The 'AoD' start to bomb to the left and right of them. As
#           one of them detect that a Angel is dead he starts to spawn
#	    another one.
#         : 0.0.2 (06/03/2000)
#	    This version only have three Angels. This is to increase the
#	    deployment and bombing speed. When there is many processes 
#           The angles spend most of their time spawning, instead of 
#	    killing other warriors:(
#         : 0.0.3 (06/03/2000)
#	    Changed the name of the END label to SE and moved it after 
#	    last instruction. Also moved the add to START and SE to after
#	    the memory location has been bombed. Why? Well, this is done
#	    to insure that the 'copy_cat' and 'parasito' warrios has a
#	    smaller chance to succeed or give away the location of the
#	    angle spawned by this Angel, mu-ha-ha:)
#         : 0.0.4 (09/03/2000)
#           Back to four angels. Increased the bombing interval to 6 from 4.
# Todo    : Test it

	title "Angels of Death 0.0.4"
	author "Jan Pool <jpool@dsp.sun.ac.za>"

START: 	data &START
SR:	move &START, START
	move &SE, SE
REP1:	info 4, ANGELS		# See how many Angels (threads) there is
	less ANGELS, 4		# Less than 4 Angles
	jump SPAWN		# Yes, then spawn another one
	move SE, [SE]		# No, then bomb the memory
	add 6, SE		# Move right
	move START, [START]
	add -6, START		# Move left
	jump REP1
SPAWN:	move &SR, SRC1		# Where to start copying
	info 2, DST1		# New random location for Angle
	move DST1, DST2		# Save that destination for the fork
	move 19, CODESZ		# Bytes to copy
REP2:	movei SRC1, DST1	# Copy the Angel to a new random location
	loop CODESZ, REP2
	fork [DST2]		# Start new Angel
	jump REP1		# This one has work to do:)
ANGELS:	data 0
SE:	data &SE
SRC1:   data 0
DST1:	data 0
DST2:	data 0
CODESZ:	data 0
