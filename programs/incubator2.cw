#
# The Incubator 2 : the empire strikes back
#

title "Incubator 2"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

R: data 0     # random number
TN: data 0    # thread number
D: data 0     # destination address
INIT:		info 0, MS
			move MS, FP
			div 3, FP
			move FP, SP
			mul 2, SP
START:	info 2, A
			own [A]
			jump START
			info 6, TM
			div 2, TM
			info 4, TN
			less TM, TN
			jump STRIKE
			info 2, R
			less R, FP
			jump STRIKE
			less R, SP
			jump SPAWN
PARASIT:	info 6, TM
			div 2, TM
			info 4, TN
			less TM, TN
			jump STRIKE
			fork [A]
			jump START
TM: data 0    # thread max number
SPAWN:	move &INIT, S			
			move A, D
			move 37 ,CS
CLOOP:	movei S, D
			loop CS, CLOOP
			jump PARASIT
S: data &INIT # source address
BOMB:		div 0, [BOMB]
CS: data 37   # interresting code size
STRIKE: 	move BOMB, [A]
			jump START
MS: data 0    # memory size
FP: data 0    # 1/3 memory size
SP: data 0    # 2/3 memory size
A: data 0     # targeted address
