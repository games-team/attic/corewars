#
# Core Wars program
#
# Static Chaos, 2000
#
Title "Stealers"
Author "Static Chaos <schaos@freemail.hu>"

CNT:   data 0
D1:    data 0
D2:    data 0
       jump SPAWN
ENTRY: move 20, CNT
NEXT:  info 2, D1
       fork [D1]
       loop CNT, NEXT
SPAWN: move &ENTRY, D2
       move 6, CNT
COPY:  movei D2, D1
       movei D2, D1
       loop CNT, COPY
       add -12, D1
       fork [D1]
       jump ENTRY
