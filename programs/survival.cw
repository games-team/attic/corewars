#
# A simple Core Wars program
#
# by FZ

	title "Survival"
	author "fzago <fzago@gresham-computing.com>"

START:	info 4, B			# get number of threads
		less B, 3			# if less than 3 threads, creates a new one
		jump FORK2
		jump START

FORK2:	info 2, D			# random number in 0...size of memory-1
		move &START, C
		move D, F
		move 12, E 			# size of the program (excluding datas)
		
COPY:	movei C, D
		loop E, COPY

		fork [F]
		jump START

B:		data 0				# number of processes
C:		data 0				# address of the beginning of the current thread
D:		data 0				# address of new process
E:		data 0				# size of the process
F:		data 0				# address of new process


