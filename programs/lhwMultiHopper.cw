#
# Core Wars program
# a simple self dublicating prgramm
#
# by Sascha Wilde, 2000
#

	title "lhwMultiHopper"
    author "Sascha Wilde <swilde@gmx.de>"

START:	move &B, B  
NEXT:	move &START, A
	add 10, B
	move B, C
COPY:	movei A, B
	own [A]
	jump COPY
	fork [C]
	add 100, B
	jump NEXT
A:	data 0
B:	data 0
C:	data 0

