#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Gemini"
        author "Walter Hofmann <walterh@gmx.de>"

START:	move &CHECK, CHECK
	info 1, ROW
	neg ROW, MROW
	add MROW, CHECK
	move 0, [CHECK]
	move &START, A
	move &START, B
	add ROW, B
	move 15, C
WAIT:	own [CHECK]
	jump WAIT
L:	movei A, B
	loop C, L
	add -15, B
	jump [B]

ROW:	data 0
MROW:	data 0
CHECK:	data 0
A:	data 0
B:	data 0
C:	data 0
