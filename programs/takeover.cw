# This one uses bi-directional advancement model using threads, however,
# It can also "capture" certain type of proggies, that will join them and
# in somecases increase the speed of advancement, althought in some cases
# it makes weired things happen

	title "Take over"
	author "Idan Sofer <sofer@ibm.net>"
PREST:  jump POSTST
START:  jump POSTST
POSTST: move &START,A
DEC:    move START,[A]
        add -1,A
FORKK:	fork M
        move START,POSTST
EX:	move REPL,FORKK
        jump DEC
REPL:   jump EX
M:      move &K, B
        jump L
A:      data 0
B:      data 0
L:      add 1,B
	move K,[B]
K:      jump L
