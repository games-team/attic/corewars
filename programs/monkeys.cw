#
# Core Wars program
#
# Daniel Karlsson, March 2000
#

	title "Twelve Monkeys"
        author "Daniel Karlsson <daniel.k@bigfoot.com>"

THR:	data	0
START:	info	2, CHILD
	move	CHILD, DST
	move	&START, SRC
	move	11, CNT
COPY:	movei	SRC, DST
	loop	CNT, COPY
	info	4, THR
	less	12, THR
	jump	[CHILD]
	fork	[CHILD]
	jump	START
CNT:	data	0
SRC:	data	0
DST:	data	0
CHILD:	data	0
