#
# Core Wars program
# 
# Hacked to Crap by: Abram Hindle March 1999
# Orignally by: Walter Hofmann, May 1999
#

	title "Quad Walker"
        author "Abram Hindle <abez@home.com>"

	move 4,D
COPY:	move 6,C
	move &START,A
	info 2,B
	move B,E
L2:	movei A,B
	loop C,L2
	fork [E]
	loop D,COPY
START:  move &START, A
	move &END, B
	move 6, C
L:	movei A, B
	loop C, L
	jump END
A:      data 0
B:      data 0
C:      data 0
D:	data 0
E:	data 0
END:	data 0
