#
# Alexey Vyskubov <alexey@pepper.spb.ru>, March 2000
#

title "Parasito Flying"
author "Alexey Vyskubov <alexey@pepper.spb.ru>"

START:	info 2, A
F:	fork [A]
	add 4, A
	jump F
A:	data 0
