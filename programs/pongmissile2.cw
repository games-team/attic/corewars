#
# Pong Missile Launcher 2
#

title "Pong Missile 2"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

C:			data 0	# random choice
P:			data &C  # missile position
D:			data &E	# missile direction address
C2:		data 0	# 1/2 mem size
C4:		data 0	# 1/4 mem size
# directions
E:			data 1
S:			data 0
O:			data -1
N:			data 0
E2:		data 1
S2:		data 0
O2:		data -1
INIT:		info 1,S
			move S, S2
			neg S, N
			info 0, C2
			div 2, C2
			move C2, C4
			div 2, C4
MLOOP:	own [P]
			jump MMOVE
			info 2, [P]
MMOVE:	add [D], P
			equal [P], 0
			jump MLOOP
			info 2, C
			less C2, C
			jump MLOOP
			less C, C4
			jump SUB
			add 1,D
			equal	&INIT, D
			move &E, D
			jump MLOOP
SUB:		add -1,D
			equal &C4, D
			move &O2, D
			jump MLOOP		
			
			
