#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Bomber bomber"
        author "Walter Hofmann <walterh@gmx.de>"

START:	info 2,B
	move &C,A
	movei A,B
	movei A,B
	movei A,B
	add -3,B
	fork [B]
	jump START

C:	info 2,A
	move 0,[A]
	jump C

A:	data 0
B:	data 0

