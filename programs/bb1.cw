	 #
	 # my first warrior
	 # have fun!
	 #
	 
	 title "Black Beast 1"
	 author "Jacek Poplawski <jacekp@linux.com.pl>"

	 jump begin
	 
p1:	 add -3,bomb
	 move bomb,[bomb]
	 loop bombs,p1

a:	 data 0
bomb:	 data &next
howmuch: data 0
bombs:	 data 0
forkbomb:fork forkbomb

begin:   info 0,howmuch
	 div 48,howmuch
	 add -3,howmuch
	 move howmuch,bombs
find:	 add 48,bomb
	 equal 0,[bomb]
	 loop howmuch,find
       	 neg howmuch,howmuch
	 add howmuch,bombs
	 mul 16,bombs
	 fork p2
	 add 30,bomb
	 jump p1

p2:	 info 2,a
	 own [a]
	 jump p2
	 move forkbomb,[a]
         info 4,a	
	 equal a,1
current: move current,next
next:	 jump p2
	
