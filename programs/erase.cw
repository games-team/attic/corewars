#
# A simple Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Memory eraser"
        author "Walter Hofmann <walterh@gmx.de>"

START:	add 1, A	# select next cell
	move 0, [A]	# clear cell
	jump START	# an endless loop
A:	data &A
