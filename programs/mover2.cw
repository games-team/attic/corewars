#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "2nd Self mover"
        author "Walter Hofmann <walterh@gmx.de>"

	fork L
	fork WALK
L:	movei A, B	# move on
	jump L
A:	data &WALK
B:	data &TO
WALK:	fork TO2
TO:	data 0
	data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
        data 0
TO2:	data 0
