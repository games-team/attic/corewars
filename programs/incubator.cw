#
# The Incubator
#

title "Incubator"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

R: data 0     # random number
TN: data 0    # thread number
D: data 0     # destination address
INIT:		info 0, MS
			info 1, SS
			move MS, HS
			div 2, HS
START:	info 2, A
			own [A]
			jump START
			info 6, TM
			div 2, TM
			info 4, TN
			less TM, TN
			jump STRIKE
			info 2, R
			less R, SS
			jump STRIKE
			less HS, R
			jump SPAWN
PARASIT:	info 6, TM
			div 2, TM
			info 4, TN
			less TM, TN
			jump STRIKE
			fork [A]
			jump START
TM: data 0    # thread max number
SPAWN:	move &INIT, S			
			move A, D
			move 36 ,CS
CLOOP:	movei S, D
			loop CS, CLOOP
			jump PARASIT
S: data &INIT # source address
BOMB:		div 0, [BOMB]
CS: data 36   # interresting code size
STRIKE: 	move BOMB, [A]
			jump START
MS: data 0    # memory size
SS: data 0    # sqrt(MS)
HS: data 0    # half memory size
A: data 0     # targeted address
