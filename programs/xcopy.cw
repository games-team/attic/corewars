#
# Core Wars program
#
# Bodo Eggert <7eggert@gmx.de>, 2000-03-02
#
	title "X-Copy"
        author "Bodo Eggert <7eggert@gmx.de>"

# some important data ;-)
A:	data 0
a1:	data 0
a2:	data 0
a3:	data 0
a4:	data 0
a5:	data 0
a6:	data 0
a7:	data 0
a8:	data 0
a9:	data 0
a10:	data 0
a11:	data 0
a12:	data 0
a13:	data 0
a14:	data 0
a15:	data 0
a16:	data 0
a17:	data 0
a18:	data 0
a19:	data 0
B:	data 0
b1:	data 0
b2:	data 0
b3:	data 0
b4:	data 0
b5:	data 0
b6:	data 0
b7:	data 0
b8:	data 0
b9:	data 0
b10:	data 0
b11:	data 0
b12:	data 0
b13:	data 0
b14:	data 0
b15:	data 0
b16:	data 0
b17:	data 0
b18:	data 0
b19:	data 0
C:	data 0
c1:	data 0
c2:	data 0
c3:	data 0
c4:	data 0
c5:	data 0
c6:	data 0
c7:	data 0
c8:	data 0
c9:	data 0
c10:	data 0
c11:	data 0
c12:	data 0
c13:	data 0
c14:	data 0
c15:	data 0
c16:	data 0
c17:	data 0
c18:	data 0
c19:	data 0
E:	data 0
d1:	data 0
d2:	data 0
d3:	data 0
d4:	data 0
d5:	data 0
d6:	data 0
d7:	data 0
d8:	data 0
d9:	data 0
d10:	data 0
d11:	data 0
d12:	data 0
d13:	data 0
d14:	data 0
d15:	data 0
d16:	data 0
d17:	data 0
d18:	data 0
d19:	data 0
F:	data 0
e1:	data 0
e2:	data 0
e3:	data 0
e4:	data 0
e5:	data 0
e6:	data 0
e7:	data 0
e8:	data 0
e9:	data 0
e10:	data 0
e11:	data 0
e12:	data 0
e13:	data 0
e14:	data 0
e15:	data 0
e16:	data 0
e17:	data 0
e18:	data 0

e19:	data 0
D:	data 0

START:	move &D, A
	info 2, B
	move B, D
	add  1,	D
	move 19, C

	own [B]
	jump START
L:	movei A, B
	loop C, L

check:	own	[D]
	jump	ok1
	jump	START

ok1:	info 4, E
	less E, 2
	jump ACT
	jump [D]
ACT:	fork [D]
	jump START

f1:	data 0
f2:	data 0
f3:	data 0
f4:	data 0
f5:	data 0
f6:	data 0
f7:	data 0
f8:	data 0
f9:	data 0
f10:	data 0
f11:	data 0
f12:	data 0
f13:	data 0
f14:	data 0
f15:	data 0
f16:	data 0
f17:	data 0
f18:	data 0
f19:	data 0
