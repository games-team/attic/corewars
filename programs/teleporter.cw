#
# Core Wars program
#
# Daniel Karlsson, March 2000
#

	title "Teleporter"
        author "Daniel Karlsson <daniel.k@bigfoot.com>"

START:	info	2, LOC
	move	9, CNT
	move	&START, SRC
	move	LOC, DST
COPY:	own	[DST]
	jump	START
	movei	SRC, DST
	loop	CNT, COPY
	jump	[LOC]	# Insert Star Trek teleporter sound here
LOC:	data	0
SRC:	data	0
DST:	data	0
CNT:	data	0
