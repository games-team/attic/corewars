# Copy Machine (Client) Ver 0.1 for corewars
#
# This is the "Client" version of the "Copy Machine".
# You may run as many copies as you like, as long as you have one server running,

	Title "Copy Machine "
	Author "Idan Sofer <sofer@ibm.net>"
A:	data 0
AA:	data 166
B:      data 0
C:	data 0
T:      data 0
CODE:	data 972
CODE2:	data 856
P:	data &P0
P0:	data 0	# Argument 1: Code
P1:	data 0	# Argument 2: Address
P2:	data 0	# Argument 3: Number of cells
P3:	data 0	# Argument 4: No. of first executive cell
P4:	data 0	# Argument 5: Where to put the jump/fork instruction
P5:	data 0 	# Argument 6: To fork?(0 = NO) # NOT YET IMPLEMENTED
L:	info 2,B
M:	move B,[AA]	# Report to server as free until gets a job
        equal CODE2,[B]
	jump MM
	jump M
MM:	equal CODE,[B]	# Wait to activation signal from the requesting proggie
	jump N
	jump MM
N:	move 5,T
K:	add 1,P		# Copy parameters from the Comm-cells
	add 1,B
	move [B],[P]
	loop T,K
	add -5,B
	add -5,P
	info 2,T
	move P1,A
	move T,C
O:	move [P1],[T]	# Now let's start to copy!
	add 1,P1
	add 1,T
	loop P2,O
	move A,P1
	move C,T
	add P4,P1
	add 1,P1
	add P3,T
	move T,X2	
	move X2,[P1]
	add -1,P1
	move X1,[P1]
	jump M
X1:	jump [X2]
X2:	data 0
