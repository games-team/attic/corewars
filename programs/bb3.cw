	  title "Black Beast 3"
	  author "Jacek Poplawski <jacekp@linux.com.pl>"
	  # warrior length is 40 cells

	  # we check 100 positions from this place      3 cells
start:	  move 100,steps
          # now we start our next tail
	  fork tail
	  # first position will be &end 
	  move &end,pos

          # ok, now we scan for non-zero-data           5 cells.
nextscan: add 7,pos
	  equal empty,[pos]
	  jump skip
	  own [pos]
	  jump skip 

	  # we found something! kill them all!!!        5 cells
	  add -5,pos
	  move 5,B
nextbomb: move empty,[pos]
	  add 2,pos
	  loop B,nextbomb

          # end of scan loop                            1 cell 
skip:	  loop steps,nextscan	  

	  # it's time to change position, let's move!   7 cells
endscan:  info 2,pos 
	  move pos,new
	  move &start,B
	  move 40,steps
copy:     movei B,pos
	  loop steps,copy
	  jump [new]
	  
          # our data - in the middle of warrior         9 cells		
new:      data 0
pos:	  data 0
empty: 	  data 0
steps:	  data 0
B:	  data 0
dst:	  data 0
forkbomb: fork forkbomb
forkbomb2:jump forkbomb
howmuch:  data 0
	  
          # tail - our little friend                    10 cells
tail:	  info 2,dst
	  own [dst]
	  jump tail
	  move forkbomb,[dst]
	  add 1,dst
	  move forkbomb2,[dst]
	  info 4,howmuch
	  equal 1,howmuch
	  # holy shit, I am alone! let's morph to imp and run away!
imp:	  move imp,nextimp
nextimp:  jump tail	  

end:	  data 0	  
	  
	  
	  
