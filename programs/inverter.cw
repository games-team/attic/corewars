#
# Inverter
#

title "Inverter"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

DB:		data 0    # data buffer
P1:		data 0    # position 1
P2:		data 0    # position 2
START:	info 2, P1
			info 2, P2
			move [P1], DB
			move [P2], [P1]
			move DB, [P2]
			jump START
