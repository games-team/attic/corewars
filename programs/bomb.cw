#
# Core Wars program
#
# Walter Hofmann, March 2000
#

	title "Bomb"
        author "Walter Hofmann <walterh@gmx.de>"

	fork BOMB
START:  info 2,A
	equal [A],B
	jump START
	own [A]
	jump START
	move 0,[A]
	add -15,A
	move 1,D
	move A,C
	jump START
BOMB:	move 0,[C]
	add D,C
	add 1,D
	jump BOMB
A:      data 0
B:      data 0
C:      data 0 
D:	data 0
