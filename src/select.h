/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __SELECT_H__
#define __SELECT_H__

#define SELECT_WINDOW_HEIGHT 350
#define SELECT_WINDOW_WIDTH 400
#define LOAD_COUNT_LINE_LENGTH 3
#define LOAD_COUNT_WIDTH 30
#define MAX_LOAD_COUNT 999

void file_selectprograms( GtkWidget *w, gpointer data );
void select_update ();
void select_update_list ();

#endif
