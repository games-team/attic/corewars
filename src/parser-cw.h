#ifndef BISON_Y_TAB_H
# define BISON_Y_TAB_H

#ifndef YYSTYPE
typedef union {
  int			integer;
  struct cw_line	*line;
  struct cw_operand	*operand;
  char			*name;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
# define	INTEGER	257
# define	IDENTIFIER	258
# define	WHITESPACE	259
# define	COMMENT	260
# define	STRING	261
# define	KEYWORD	262


extern YYSTYPE cw_lval;

#endif /* not BISON_Y_TAB_H */
