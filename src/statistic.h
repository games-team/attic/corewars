/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __STATISTIC_H__
#define __STATISTIC_H__

#ifndef CMD_LINE
#define STATISTIC_WINDOW_HEIGHT 200
#define STATISTIC_TITLE_WIDTH 140
#define STATISTIC_AUTHOR_WIDTH 220
#define STATISTIC_BULLET_SIZE 10
#define STATISTIC_FULL_REDRAW_COUNT 10

void statistic_view ();
void statistic_update (int partial);
void statistic_clear ();
void statistic_select (int x, int y);
#endif

int process_score (struct process *p);
int process_compare (const void *a, const void *b);

#endif
