/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __PROGRAM_H__
#define __PROGRAM_H__

#include <sys/types.h>

#ifndef CMD_LINE
#include <gtk/gtk.h>
#endif

#define STANDARD_LOAD_COUNT 0

#define AM_ADDRESS	0
#define AM_CONSTANT	1
#define AM_VALUE	2
#define AM_INDIRECT	3
#define AM_STRING	4

#define CMD_TITLE	0
#define CMD_AUTHOR	1
#define CMD_DATA	2
#define CMD_MOVE	3
#define CMD_ADD		4
#define CMD_NEG		5
#define CMD_AND		6
#define CMD_NOT		7
#define CMD_MUL		8
#define CMD_DIV		9
#define CMD_MOD		10
#define CMD_EQUAL	11
#define CMD_LESS	12
#define CMD_JUMP	13
#define CMD_FORK	14
#define CMD_INFO	15
#define CMD_SYSTEM	16
#define CMD_OWN		17
#define CMD_LOOP	18
#define CMD_MOVEI	19

struct program {
  struct program *next, *prev;
  char *filename;
  dev_t device;
  ino_t inode;
  time_t mtime;
  char *error;
  char *title;
  char *author;
  int load_count, tournament_count;
  int tournament_score, tournament_games;
  int language;
  union {
    struct {
      int opcode_number;
      struct cw_line **code;
    } cw;
    struct {
      struct rc_command *obj;
      int org;
      int size;
    } rc;
  } lang;
#ifndef CMD_LINE
  GtkWidget *entry;
#endif
} ;

extern struct program *program_list_root;

void unload_program (struct program *p);
int load_program (struct program *p);
void unlink_and_free_program (struct program *p);
struct program *alloc_and_link_program ();

#ifndef CMD_LINE
void recheck_program_list ();
int add_program_directory( const char *directory );
void program_update ();
#endif

#endif
