/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __OPTIONS_H__
#define __OPTIONS_H__

#ifndef CMD_LINE
#include <gtk/gtk.h>
#endif

#define OPTIONS_ENTRY_WIDTH 75

#define LANGUAGE_NONE 0
#define LANGUAGE_CW 1
#define LANGUAGE_RC 2

#define LANGUAGE_STANDARD LANGUAGE_CW

#define SIZE_STANDARD 6400
#define SIZE_MAX 1000000
#define SIZE_MIN 625
#define MAX_THREADS_STANDARD 1000
#define MAX_CYCLES_STANDARD 1000000
#define MAX_LENGTH_STANDARD 200
#define MIN_DISTANCE_STANDARD 100
#define STOP_EARLY_STANDARD 1

#define ALLOW_SELF_MOVE_STANDARD 1

#define PIXMAP_SIZE (6*DIMENSION_MAX+1)
#define EXECUTE_DELAY_STANDARD 50
#define EXECUTE_DELAY_MIN 5
#define EXECUTE_UPDATE_STANDARD 500
#define STATISTIC_DELAY_STANDARD 500
#define STATISTIC_DELAY_MIN 50
#define SCORE_ALIVE_STANDARD 1
#define SCORE_CELLS_STANDARD 1
#define SCORE_KILLS_STANDARD 100

#define EDIT_COMMAND_STANDARD "nedit"

#ifndef CMD_LINE
void view_options (GtkWidget *w, gpointer data);
void options_update ();
#endif

extern int LANGUAGE;

extern int SIZE;
extern int DIMENSIONX;
extern int DIMENSIONY;
extern int MAX_THREADS;
extern int MAX_CYCLES;
extern int MAX_LENGTH;
extern int MIN_DISTANCE;
extern int STOP_EARLY;

extern int ALLOW_SELF_MOVE;

extern int EXECUTE_DELAY;
extern int EXECUTE_UPDATE;
extern int STATISTIC_DELAY;
extern int SCORE_ALIVE;
extern int SCORE_CELLS;
extern int SCORE_KILLS;
extern int SCORE_RANK;
extern int SCORE_BEST;

extern char EDIT_COMMAND[1024];

#endif
