/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MAIN_GUI_H__
#define __MAIN_GUI_H__

#include <gtk/gtk.h>

extern GtkWidget *menu_file_start;
extern GtkWidget *menu_file_pause;
extern GtkWidget *menu_file_continue;
extern GtkWidget *menu_file_step;
extern GtkWidget *menu_file_stop;
extern GtkWidget *menu_view_gstatistic;
extern GtkWidget *menu_view_pstatistic;
extern GtkWidget *menu_tournament_start;
extern GtkWidget *menu_tournament_stop;

extern GtkWidget *main_window;
extern GtkWidget *main_vbox;
extern GtkWidget *main_drawing_area;
extern GdkDrawable *main_map;

void create_drawing_area ();
int main_gui(int argc,char *argv[]);
void menu_start_update ();

#endif
