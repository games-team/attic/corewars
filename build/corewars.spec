%define	name	corewars
%define	version	0.9.13
%define	release	1

Summary:	Program fighting
Name:		%{name}
Version:	%{version}
Release:	%{release}
Group:		Games/Strategy
Copyright:	GPL
Url:		http://corewars.sourceforge.net/
Source:		%{name}-%{version}.tar.gz
Buildroot:	/var/tmp/%{name}-buildroot

%description
Core Wars is a simulation game. A number of programs are loaded into a
virtual machine and executed. The programs can to crash each other,
manipulate other programs, overwrite as much memory as they can, etc...
The "best" program is selected according to a scoring mechanism which
involves the number of memory cells the program has overwritten, the
number of other programs it has crashed and whether/when it crashed.   

%prep
rm -rf $RPM_BUILD_ROOT

%setup
rm -rf $RPM_BUILD_ROOT

%configure --prefix=/usr

%build
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS"

if [ -x /usr/bin/getconf ] ; then
    NRPROC=$(/usr/bin/getconf _NPROCESSORS_ONLN)
    if [ $NRPROC -eq 0 ] ; then
        NRPROC=1
    fi
else
    NRPROC=1
fi  

make -j $NRPROC && mv mtr xmtr && make distclean

%configure --prefix=/usr --disable-gtk
make

%install
make DESTDIR=$RPM_BUILD_ROOT install-strip

#%clean
#if [ -d $RPM_BUILD_ROOT ]; then rm -r $RPM_BUILD_ROOT; fi

%files
%defattr(-,root,root)
%dir /usr/share/corewars
%doc ChangeLog COPYING README INTERESTING-COMBINATIONS DIFFERENCES
/usr/bin/*
/usr/share/corewars/*
/usr/man/man6/*

%changelog
* Wed Apr 12 2000 Walter Hofmann <walterh@gmx.de>
- install man page
* Mon Mar 13 2000 Walter Hofmann <walterh@gmx.de>
- new in corewars main source
- tried to eliminate mandrake-isms
* Mon Mar 06 2000 Lenny Cartier <lenny@mandrakesoft.com>
- new in contribs
- mandrake build
